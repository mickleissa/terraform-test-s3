terraform {
    required_providers {
        aws= {
            source ="hashicorp/aws"
            version = "2.7"
        }
    }
}

provider "aws" {
    region = "us-east-2"
    }

resource "aws_instance" "prodbox" {
    ami = "ami-05d72852800cbf29e"
    instance_type = "t2.micro"


}