terraform {
    required_providers {
        aws= {
            source ="hashicorp/aws"
        }
    }
}

provider "aws" {
    region = "us-east-1"
    }

resource "aws_instance" "prodbox" {
    ami                  = "ami-05d72852800cbf29e"
    instance_type        = "t2.micro"
}