terraform {
    backend "s3" {
        bucket = "s3-backend-terraform-test"
        key = "s3backendterraformtest.tfstate"
        region = "us-east-2"
        //profile = "Terraform001"
    }
}

# terraform {
#   backend "s3" {
#     bucket = "mybucket"
#     key    = "path/to/my/key"
#     region = "us-east-1"
#   }
# }